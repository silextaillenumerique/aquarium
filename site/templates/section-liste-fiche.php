<?php snippet('header') ?>

<?php snippet('site-menu') ?>

<?php $section = $page->parents()->findBy('intendedTemplate', 'section') ?>
<main id="site-content" role="main" aria-label="<?= t('label.main') ?>" class="pa10 pa20-sm pa40-md" >
	<div class="page-block">
		<div class="columns">
			<div class="col col-12 col-6-md">
				<article class="mb60">
					<header class="row--large">
						<div class="mb15">
							<?php snippet('breadcrumbs') ?>
						</div>
						<h1 class="row"><?= $page->title()->kt() ?></h1>
						<?php if ($page->description()->isNotEmpty()): ?>
							<div class="row text--large">
								<?= $page->description()->widont() ?>
							</div>
						<?php endif ?>
					</header>
					
					<?php snippet('page-intro') ?>
					<?php snippet('page-text') ?>
					<?php snippet('page-links') ?>
					
				</article>
			</div>
			<div class="col col-12 col-6-md">
				<?php snippet('page-embed') ?>
				<?php snippet('page-gallery') ?>
			</div>
		</div>

		<?php snippet('page-prevnext') ?>
	</div>
</main>

<?php snippet('footer') ?>
