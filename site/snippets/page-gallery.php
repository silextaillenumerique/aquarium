<?php if ($page->gallery()->isNotEmpty() && $images = $page->gallery()->toFiles()): ?>
	<?php foreach ($page->gallery()->toFiles() as $image): ?>
		<figure class="row--medium">
			<img src="<?= $image->thumb('side_picture')->url() ?>" srcset="<?= $image->srcset('default') ?>" alt="<?= $image->description() ?>">
			<?php if ($image->legend()->isNotEmpty()): ?>
				<figcaption class="mt5 text--small"><?= $image->legend() ?></figcaption>
			<?php endif ?>
		</figure>
	<?php endforeach ?>
<?php endif ?>