<?php if ($page->text()->isNotEmpty()): ?>
	<div class="row text">
		<?= $page->text()->kt() ?>
	</div>
<?php endif ?>
<?php if ($page->duree()->isNotEmpty() || $page->public()->isNotEmpty()): ?>
	<div class="mt40 mb40 text--small">
		<?php if ($page->duree()->isNotEmpty()): ?>
		<div>
		Durée : <?= $page->duree() ?>
		</div>
		<?php endif ?>
		<?php if ($page->public()->isNotEmpty()): ?>
		<div>
		<?= $page->public() ?>
		</div>
		<?php endif ?>
	</div>
<?php endif ?>