<?php snippet('header') ?>

<?php snippet('site-menu') ?>

<?php $section = $page->parents()->findBy('intendedTemplate', 'section') ?>
<main id="site-content" role="main" aria-label="<?= t('label.main') ?>" class="pa10 pa20-sm pa40-md" >
	<div class="page-block">
		<div class="columns">
			<aside class="col col-12 col-4-md order-2-md">
				<?php snippet('section-sidebar', ['section' => $section]) ?>
			</aside>

			<div class="col col-12 col-8-md order-1-md">
				<article class="mb60">
					<header class="row--large">
						<div class="mb15">
							<?php snippet('breadcrumbs') ?>
						</div>
						<h1 class="row"><?= $page->title()->widont() ?></h1>
						<?php if ($page->description()->isNotEmpty()): ?>
							<div class="row text--large">
								<?= $page->description()->widont() ?>
							</div>
						<?php endif ?>
					</header>
					
					
					<?php snippet('page-intro') ?>
					<?php snippet('page-text') ?>
				</article>
			</div>
		</div>
		<div>
			<?php if ($page->listTitle()->isNotEmpty()): ?>			
				<h2 class="mb15"><?= $page->listTitle()->widont() ?></h2>
			<?php endif ?>
			<?php snippet('cards', ['items' => $page->children()->listed()]) ?>
		</div>
	</div>
</main>

<?php snippet('footer') ?>
