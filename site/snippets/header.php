<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    
    <meta name="robots" content="index,follow">

    <link rel="apple-touch-icon" sizes="180x180" href="<?= asset('assets/images/favicons/apple-touch-icon.png')->url() ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= asset('assets/images/favicons/favicon-32x32.png')->url() ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= asset('assets/images/favicons/favicon-16x16.png')->url() ?>">
    <link rel="manifest" href="<?= asset('assets/images/favicons/site.webmanifest')->url() ?>">

    <?php echo $page->metaTags() ?>

    <?= Bnomei\Fingerprint::css('assets/style.css') ?>
    <?= Bnomei\Fingerprint::js('assets/jquery/jquery-3.6.0.min.js') ?>

    <style>

        /* Hack to prevent a FOUC (Flash Of Unstyled Content) */
        .no-fouc {
            opacity: 0;
            transition: opacity .3s;
        }
    </style>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PZKMTZS');</script>
<!-- End Google Tag Manager -->

</head>

<body class="no-fouc">
     <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PZKMTZS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

