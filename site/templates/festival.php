<?php snippet('header') ?>

<?php snippet('site-menu') ?>

<main id="site-content" class="pa10 pa20-sm pa40-md" role="main" >

	<div class="page-block">

		<header class="row--large">
			<h1><?= $page->title()->widont() ?></h1>
		</header>

		<?php if ($page->children()->listed()->count() && $editions = $page->children()->listed()): ?>
			<div class="published mb60">
				<?php foreach ($editions as $edition): ?>
					<?php $isAccessible = $edition->programmationIsAccessible()->toBool() ?>
					<section class="row row--large">
						<div class="columns">
							<div class="col col-12 col-6-md mb15 order-2 order-1-md">
								<header class="row">
									<?= r($isAccessible, '<a class="unstyled block" href="'.$edition->url().'">') ?>
										<h2 class="row"><?= $edition->title()->widont() ?></h2>
										<?php if ($edition->description()->isNotEmpty()): ?>
											<div class="text--large"><?= $edition->description() ?></div>
										<?php endif ?>
										<?php if ($edition->dates()->isNotEmpty()): ?>
											<div class="text--large"><?= $edition->dates() ?></div>
										<?php endif ?>
									<?= r($isAccessible, '</a>') ?>
								</header>

								<?php snippet('page-text', ['page' => $edition]) ?>

								<?php if ($isAccessible): ?>
									<div class="text--extralarge">
										<a class="unstyled" href="<?= $edition->url() ?>">
											<div class="button button--small rounded title-h3 mr15">+</div><span class="link">La programmation du festival</span>
										</a>
									</div>
								<?php endif ?>
							</div>
							<div class="col col-12 col-6-md mb15 order-1 order-2-md">
								<?php if ($edition->picture()->isNotEmpty() && $image = $edition->picture()->toFile()): ?>
									<?= r($isAccessible, '<a class="unstyled" href="'.$edition->url().'">') ?><?php /*
										*/?><figure class="inline-block">
											<img src="<?= $image->thumb('side_picture')->url() ?>" alt="<?= $image->description() ?>">
											<?php if ($image->legend()->isNotEmpty()): ?>
												<figcaption class="mt5 text--small"><?= $image->legend() ?></figcaption>
											<?php endif ?>
										</figure><?php /*
								*/?><?= r($isAccessible, '</a>') ?>
								<?php endif ?>
							</div>
						</div>
					</section>
				<?php endforeach ?>
			</div>
		<?php endif ?>

		<?php if ($page->children()->unlisted()->count() && $editions = $page->children()->unlisted()->sortBy('startdate', 'desc')): ?>
			<div class="archive mb60">
				<?php foreach ($editions as $edition): ?>
					<?php $isAccessible = $edition->programmationIsAccessible()->toBool() ?>
					<section class="row row--large">
						<div class="columns columns--small">
							<div class="col col-12 col-2-sm">
								<?php if ($edition->picture()->isNotEmpty() && $image = $edition->picture()->toFile()): ?>
									<?= r($isAccessible, '<a class="unstyled" href="'.$edition->url().'">') ?>
										<figure><img src="<?= $image->thumb('columns columns--small')->url() ?>" alt="<?= $image->description() ?>"></figure>
									<?= r($isAccessible, '</a>') ?>
								<?php endif ?>
							</div>
							<div class="col col-12 col-10-sm">
								<?= r($isAccessible, '<a class="unstyled" href="'.$edition->url().'">') ?>
									<h2 class="row--extrasmall"><?= $edition->title()->widont() ?></h2>
									<?php if ($edition->description()->isNotEmpty()): ?>
										<div class="text--large"><?= $edition->description() ?></div>
									<?php endif ?>
								<?= r($isAccessible, '</a>') ?>
							</div>
						</div>
					</section>
				<?php endforeach ?>
			</div>
		<?php endif ?>

	</div>
</main>
<?php snippet('footer') ?>
