
<?php if ($page->animations()->isNotEmpty()): ?>
	<div class="row--extralarge">
		<h2 class="mb15">En complément</h2>
		<div class="text">
			<?= $page->animations()->kt() ?>
		</div>
	</div>
<?php endif ?>