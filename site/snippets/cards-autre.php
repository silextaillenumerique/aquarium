
<?php if (isset($items) && count($items)): ?>
	<div class="columns">
		<?php foreach ($items as $item): ?>
			<div class="col col-12 col-6-sm col-4-md mb20 relative">
				<a class="unstyled block pb20" href="<?= $item->url() ?>">
					<?php if ($item->picture()->isNotEmpty() && $image = $item->picture()->toFile()): ?>
						<figure class="shaded">
							<img src="<?= $image->focusCrop(900)->url() ?>" alt="<?= $image->description() ?>">
							<?php if ($item->mot()->isNotEmpty()): ?>
								<div class="mot upper title-h3">
									<span><?= $item->mot() ?></span>
								</div>
							<?php endif ?>
						</figure>
						<div class="pa5 pa0-md pt15-md grey-mode normal-mode-md">
							<div class="row--extrasmall">
								<h3 class="title-h2"><?= $item->title()->widont() ?></h3>
							</div>
							<?php if ($item->dates()->isNotEmpty()): ?>
							<div class="row--extrasmall"><?= $item->dates() ?></div>
							<?php endif ?>
						</div>
					<?php endif ?>
				</a>
			</div>
		<?php endforeach ?>
	</div>
<?php endif ?>