<?php
	$tmpDate = getdate(0);
	$currentDate = getdate();
?>

<script>
$(document).ready(function () {

	/* jquery open calendar - édition bruit */

	$('.display_agenda').click( function (){
		if ($(this).hasClass('closed')){
			$(this).removeClass('closed');
			$(this).addClass('opened');
			$('.display_agenda span.closed').addClass('hidden');
			$('.display_agenda span.opened').removeClass('hidden');
			$('.days').removeClass('hidden');

		} else {
			$(this).removeClass('opened');
			$(this).addClass('closed');
			$('.display_agenda span.closed').removeClass('hidden');
			$('.display_agenda span.opened').addClass('hidden');
			$('.days').addClass('hidden');
		}
	});

}); // fin doc ready
</script>


<div class="calendar">
	<?php
		if (!$calendar->getAllEvents()):
			echo t('calendar-no-entry');
		else:
	?>
	<div class="text--small upper mb10">
		<a class="display_agenda closed">
				<span class="svg">
					<svg width="9px" height="10px" viewBox="0 0 9 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
							<g id="fiche-spectacle-dans-bruit" transform="translate(-296.000000, -343.000000)">
							    <g id="Group" transform="translate(295.000000, 343.000000)">
									<line x1="5.5" y1="9.5" x2="9.5" y2="6.5" id="Line-3" stroke="#000000" transform="translate(7.500000, 8.000000) rotate(-8.130102) translate(-7.500000, -8.000000) "></line>
									<line x1="1.5" y1="9.5" x2="5.5" y2="6.5" id="Line-3" stroke="#000000" transform="translate(3.500000, 8.000000) scale(1, -1) rotate(-8.130102) translate(-3.500000, -8.000000) "></line>
									<line x1="5.5" y1="0.5" x2="5.5" y2="9.5" id="Line-2" stroke="#000000"></line>
								</g>
							</g>
						</g>
					</svg>
				</span>
				<span class="closed"><?php echo t('date_close'); ?></span>
				<span class="opened hidden"><?php echo t('date_open'); ?></span>
		</a>
	</div>
	<div class="days hidden bb b2">
			<?php $i = 0; ?>
			<?php foreach ($calendar->getAllEvents() as $event):
				$date = $event->getBeginDate();
			?>
				<?php $i++; ?>
				<?php 	if (($tmpDate['mday'] != $date['mday']) || ($i == 1)) : ?>
					<?php if ($i !== 1) : ?>
						</div> <!-- fin bl-event -->
					</div> <!-- fin prog -->
					<?php 	endif; ?>
					<?php 	if ($tmpDate['month'] != $date['month'] || ($i == 1)) : ?>
					<?php if ($i !== 1) : ?>
						</div> <!-- fin bl-days -->
					</div> <!-- fin bl-months -->
					<?php 	endif; ?>
					<div class="bl-month bt b2 flex wrap">
						<div class="month upper col-12 col-2-sm  pt5">
							<?php echo strftime(t('calendar-time-format'), $date[0]); ?>	
						</div>
						<div class="bl-days col-12 col-10-sm">
						<?php 	endif; ?>
							<div class="prog flex wrap bb b1 pt5<?php e($event->isPast(), ' past'); ?>
							<?php if (($date['weekday'] == 'Saturday') || ($date['weekday'] == 'Sunday')) :
							echo ' we' ?>
							<?php 	endif; ?> ">
								<div class="jour upper col-12 col-2-sm">
								<?php echo strftime(t('calendar-day-format'), $date[0]); ?>
								</div>
								<div class="bl-event col-12 col-10-sm">
								<?php 	endif; ?>
									<!-- event -->
									<div class="mb5 event flex wrap">
									<!-- time event -->
										<div class="col-12 col-3-sm">
											<?php echo $event->getBeginHtml(); ?>	
										</div>
										<?php 	foreach ($fields as $key => $value): ?>
										<?php $event_links[] = $event->getField($key); ?>
										<?php 	endforeach; ?>
										<!-- link event -->
										<div class="col-12 col-7-sm">
											<span class="upper"><a href="<?php echo $event_links[1]; ?>"><?php echo $event_links[0]; ?></a></span>
										</div>
										<!-- link resa -->
										<div class="col-12 col-2-sm resa mt5 mt0-sm">
											<?php if (!empty($event_links[2])): ?>
											<a href="<?php echo $event_links[2]; ?>" target="_blank">
											<!-- bt resa -->
											<span class="button button--small valid text--small">Réserver</span></a>
											<?php endif ?>
										</div>
									</div> <!-- fin event -->
								<?php unset($event_links) ?>			
								<?php $tmpDate = $date; ?>
								<?php endforeach; ?>
		    					</div> <!-- fin bl-event end -->
							</div> <!-- fin prog end -->
							<?php endif; ?>
						</div>  <!-- fin bl-days end -->
					</div> <!-- fin bl-months end -->
		</div> <!-- fin days -->
</div>  <!-- fin calendar  -->
