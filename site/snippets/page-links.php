<?php if ($page->links()->isNotEmpty()): ?>
	<div class="my60">
		<h2 class="mb15">Téléchargement et liens</h2>
		<?php foreach ($page->links()->toStructure() as $link): ?>
			<?php $link = $link->link()->toLinkObject(); ?>
			<?php if ($link): ?>
				<div class="row">
					<a <?= $link->attr([]) ?> class="unstyled inline-flex vertical-center">
						<?php if ($link->type() == 'file'): ?>
							<img class="mr5 icon" src="<?= asset('assets/images/icons/file.svg')->url() ?>">
						<?php elseif ($link->type() == 'url'): ?>
							<img class="mr5 icon" src="<?= asset('assets/images/icons/link.svg')->url() ?>">
						<?php elseif ($link->type() == 'page'): ?>
							<img class="mr5 icon" src="<?= asset('assets/images/icons/page.svg')->url() ?>">
						<?php endif ?>
						<span class="link"><?= $link->title() ?></span>
					</a>
				</div>
			<?php endif ?>
		<?php endforeach ?>
	</div>
<?php endif ?>