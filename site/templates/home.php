<?php snippet('header') ?>

<?php snippet('homepage-cover') ?>

<div id="infos">
	
	<?php snippet('site-menu') ?>

	<main id="site-content" class="pa10 pa20-sm pa40-md" role="main" >
		<div class="page-block">
			
			<?php if ($page->selection()->isNotEmpty() && $selection = $page->selection()->toFiles()): ?>
				<section class="py0 py60-md mt30">
					<div class="columns">
						<?php foreach ($selection as $item): ?>
							<?php if ($item->link()->isNotEmpty() && $related = $item->link()->toLinkObject()): ?>
							<?php $link = $item->link()->toLinkObject(); ?>
								<div class="col col-12 col-6-sm col-4-md mb20">
									<a <?= $link->attr([]) ?> class="unstyled block relative">
										<figure class="shaded">
											<img class="cover" src="<?= $item->focusCrop(900)->url() ?>" alt="<?= $item->description() ?>">
										</figure>
										<div class="pt10">
											<div class="row--extrasmall"><h2><?= $item->title()->widont() ?></h2></div>
											<div class="row--small text"><?= $item->description()->widont() ?></div>
											<div class="button button--small rounded title-h3 yellow-bg">+</div>
										</div>
									</a>
								</div>
							<?php else: ?>
								<div class="col col-12 col-6-sm col-4-md mb20">
									<div class="unstyled block relative">
										<figure class="shaded">
											<img class="cover" src="<?= $item->focusCrop(900)->url() ?>" alt="<?= $item->description() ?>">
										</figure>
										<div class="pt10">
											<div class="row--extrasmall"><h2><?= $item->title()->widont() ?></h2></div>
											<div class="row--small text"><?= $item->description()->widont() ?></div>
										</div>
									</div>
								</div>
							<?php endif ?>
						<?php endforeach ?>
					</div>
				</section>
			<?php endif ?>

		</div>
	</main>
</div>

<?php snippet('footer') ?>
