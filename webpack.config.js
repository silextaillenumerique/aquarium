const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const RemovePlugin = require('remove-files-webpack-plugin')
const autoprefixer = require('autoprefixer');

module.exports = env => {
	const isDevMode = env.NODE_ENV === 'development'
	console.log(isDevMode ? '🔨    Running in development mode' : '🚀    Running in production mode')
	return {
		entry: ['./src/js/index.js'],
		output: {
			filename: 'script.js',
			path: path.join(__dirname, 'assets')
		},
		module: {
			rules: [
				{
					test: /\.(js|jsx)$/,
					exclude: /node_modules/,
					use: ['babel-loader']
				},
				{
					test: /\.scss$/,
					resolve: {
						extensions: ['.scss', '.sass']
					},
					use: [
						{
							loader: MiniCssExtractPlugin.loader,
							options: {
								publicPath: path.join(__dirname, 'assets/style.css')
							}
						},
						{
							loader: 'css-loader',
							options: {
								importLoaders: 1,
								url: false,
								sourceMap: true
							}
						},
						{
							loader: 'postcss-loader',
							options: {
								config: {
									path: path.join(__dirname, 'postcss.config.js')
								}
							}
						},
						{
							loader: 'sass-loader',
							options: {
								sourceMap: true
							}
						}
					]
				},
				{
					test: /\.css$/,
					use: ['style-loader', 'css-loader']
				},
				{
					test: /\.(png|woff|woff2|eot|ttf|svg)$/,
					loader: 'url-loader?limit=100000'
				}
			]
		},
		plugins: [
			new MiniCssExtractPlugin({
				filename: 'style.css'
			}),
			new RemovePlugin({
				before: {
					test: [
						{
							folder: path.join(__dirname, 'assets'),
							method: filePath => {
								return new RegExp(/\.map$/, 'm').test(filePath)
							}
						}
					]
				}
			})
		],
		watch: isDevMode,
		devtool: isDevMode ? 'source-map' : false
	}
}
