<?php if ($page->seances()->isNotEmpty()): ?>
	<div class="my40 bb b1">
		<h2 class="mb15">Séances et réservations</h2>
		<?php foreach ($page->seances()->toStructure() as $seance): ?>
			<div class="flex bt b1 py5">
				<div class="no-shrink mr10 py5 text--large"><?= $seance->date() ?></div>
				<?php if ($seance->link()->isNotEmpty()): ?>
					<div class="self-align-right">
						<a href="<?= $seance->link() ?>" target="_blank" class="unstyled block"><span class="button button--small valid">Réserver</span></a>
					</div>
				<?php endif ?>
			</div>
		<?php endforeach ?>
	</div>
<?php endif ?>