
<?php if ($page->children()->listed()->count()) {
    go($page->children()->first()->url());
} ?>

<?php snippet('header') ?>

<?php snippet('site-menu') ?>

<main id="site-content" role="main" aria-label="<?= t('label.main') ?>" class="pa10 pa20-sm pa40-md" >
	<div class="page-block">
		<header class="row--large">
			<h1><?= $page->title()->widont() ?></h1>
		</header>
		<div class="text">
	    	Cette section est vide.
		</div>
	</div>
</main>
<?php snippet('footer') ?>
