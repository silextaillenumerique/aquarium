<?php snippet('header') ?>

<?php snippet('site-menu') ?>

<main id="site-content" class="pa10 pa20-sm" role="main" >

	<div class="page-block">
		<section class="columns mb60">
			<div class="col col-12 col-6-md mb20 pa40-md">
				<header class="row--large">
					<div class="mb15"><?php snippet('breadcrumbs') ?></div>
					<h1 class="row"><?= $page->title()->kt() ?></h1>
					<div class="metas row--extrasmall text--large">
						<?php if ($page->description()->isNotEmpty()): ?>
						<div class="text--large"><?= $page->description() ?></div>
						<?php endif ?>
						<?php if ($page->genre()->isNotEmpty()): ?>
							<div class="upper"><?= $page->genre() ?></div>
						<?php endif ?>
						<?php if ($page->dates()->isNotEmpty()): ?>
							<div class="relative"><?= $page->dates() ?>
							<!-- bt meme soir -->
								<?php if ($page->related()->isNotEmpty()): ?>
									<a href="#link-related" class="link-related text--small">
										<span class="svg">
											<svg width="9px" height="10px" viewBox="0 0 9 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
											    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
											        <g id="fiche-spectacle-dans-bruit" transform="translate(-296.000000, -343.000000)">
											            <g id="Group" transform="translate(295.000000, 343.000000)">
											                <line x1="5.5" y1="9.5" x2="9.5" y2="6.5" id="Line-3" stroke="#000000" transform="translate(7.500000, 8.000000) rotate(-8.130102) translate(-7.500000, -8.000000) "></line>
											                <line x1="1.5" y1="9.5" x2="5.5" y2="6.5" id="Line-3" stroke="#000000" transform="translate(3.500000, 8.000000) scale(1, -1) rotate(-8.130102) translate(-3.500000, -8.000000) "></line>
											                <line x1="5.5" y1="0.5" x2="5.5" y2="9.5" id="Line-2" stroke="#000000"></line>
											            </g>
											        </g>
											    </g>
											</svg>
										</span>
										<span>le même soir</span>
									</a>
								<?php endif ?>
							<!-- bt resa -->
								<?php if ($page->btIsAccessible()->toBool() === true): ?>
									<?php if ($page->reservation()->isNotEmpty()): ?>
									<a class="unstyled" href="<?= $page->reservation()->url() ?>" target="_blank"><span class="button button--small valid text--small">Réserver</span></a>
									<?php endif ?>
								<?php endif ?>
							</div>
						<?php endif ?>
						<?php if ($page->accroche()->isNotEmpty()): ?>
							<div class="mt40"><?= $page->accroche()->kt() ?></div>
						<?php endif ?>
						<?php if ($page->avec()->isNotEmpty()): ?>
							<div><?= $page->avec()->kt() ?></div>
						<?php endif ?>
					</div>
				</header>

				<?php snippet('page-text') ?>
				<?php snippet('page-details') ?>
				<?php snippet('page-seances') ?>
				<?php snippet('page-prices') ?>
				<?php snippet('page-animations') ?>
				<?php snippet('page-links') ?>

			</div>

			<div class="col col-12 col-6-md">
				<?php snippet('page-embed') ?>
				<?php snippet('page-gallery') ?>
			</div>

		</section>

		<section class="mb40 mt100 related">
			<?php snippet('page-related') ?>
			<?php snippet('page-prevnext') ?>
		</section>
	</div>
</main>
<?php snippet('footer') ?>
