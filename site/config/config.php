<?php
return [
    'debug' => true,
    'languages' => true,
    'date.handler'  => 'strftime',
    'locale' => 'fr_FR.utf-8',
    'cache' => [
        'pages' => [
            'active' => false
        ]
    ],
    'bnomei.fingerprint.https' => false,
    'thumbs' => [
        'presets' => [
            'default' => ['width' => 960, 'height' => 540],
            'preview' => ['width' => 600, 'height' => 500],
            'side_picture' => ['width' => 1280, 'height' => 840],
            'small_preview' => ['width' => 600, 'height' => 600],
            'portrait' => ['width' => 500, 'height' => 600],
            'thumb' => ['width' => 250, 'height' => 300],
            'cropped_portrait' => ['width' => 500, 'height' => 600, 'crop' => true],
            'cropped_square_large' => ['width' => 900, 'height' => 900, 'crop' => true],
            'cropped_square' => ['width' => 600, 'height' => 600, 'crop' => true],
            'cropped_thumb' => ['width' => 300, 'height' => 300, 'crop' => true],
            'cover' => ['height' => 1000, 'width' => 1600,'quality' => 70 ]
        ],
        'srcsets' => [
            'default' => [320, 640, 960, 1280],
            'cover' => [320, 640, 960, 1280, 1400, 1740]
        ]
    ],
    'omz13.xmlsitemap.hideuntranslated' => true,
    'omz13.xmlsitemap.includeUnlistedWhenTemplateIs' => ['home', 'default'],
    'pedroborges.meta-tags.default' => function ($page, $site) {
        return [
            'title' => $page->isHomePage() ? $site->title() : $page->title(),
            'meta' => [
                'description' => function ($page, $site) {
                    if ($page->isHomePage() && $site->reseau()->isNotEmpty()) {
                        return $site->reseau();
                    } elseif ($page->isHomePage() == false && $page->reseau()->isNotEmpty()) {
                        return $page->reseau();
                    } elseif ($page->isHomePage() == false && $page->reseau()->isNotEmpty() == false) {
                        return $site->title();
                    } else {
                        return null;
                    }
                }
            ],
            'link' => [
                'canonical' => $page->url(),
            ],
            'og' => [
                'type' => 'website',
                'title' => $page->isHomePage() ? $site->title() : $page->title(),
                'site_name' => $site->title(),
                'url' => $page->url(),
                'locale' => 'fr_FR',
                'description' => function ($page, $site) {
                    if ($page->isHomePage() && $site->reseau()->isNotEmpty()) {
                        return $site->reseau();
                    } elseif ($page->isHomePage() == false && $page->reseau()->isNotEmpty()) {
                        return $page->reseau();
                    } elseif ($page->isHomePage() == false && $page->reseau()->isNotEmpty() == false) {
                        return $site->title();
                    } else {
                        return null;
                    }
                },
                'namespace:image' => function ($page, $site) {
                    if (($page->picture()->isNotEmpty() && $image = $page->picture()->toFile()) || ($site->picture()->isNotEmpty() && $image = $site->picture()->toFile())) {
                        $thumb = $image->thumb(['width' => 1200, 'height' => 630, 'crop' => true ]);
                        return [
                            'image' => $thumb->url(),
                            'height' => $thumb->height(),
                            'width' => $thumb->width(),
                            'type' => $image->mime()
                        ];
                    } else {
                        return null;
                    }
                }
            ],
            'twitter' => [
                'card' => 'summary_large_image',
                'site' => $site->twitterAccount(),
                'domain' => $site->url(),
            ]
        ];
    }
];

