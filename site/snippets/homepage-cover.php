<?php if (page('home')->picture()->isNotEmpty() && $image = page('home')->picture()->toFile()): ?>
	<div id="homepage-cover" aria-hidden="true">
		<figure>
			<img src="<?= $image->focusCrop(2400, 2400)->url() ?>" 
				srcset="<?= $image->srcset('cover') ?>" alt="<?= $image->description() ?>"
				style="object-fit: cover; object-position: <?php echo $image->focusPercentageX() ?>% <?php echo $image->focusPercentageY() ?>%; width: 100%; height: 100%;"
			/>

		</figure>

		<a href="#infos" class="unstyled text-container pa10 pa20-md flex vertical-center center no-hover">
			<?php if ($page->layer()->isNotEmpty() && $layer = $page->layer()->toFile()): ?>
				<img class="scale-down" role="logo" src="<?= $layer->url() ?>" alt="Logotype">
			<?php endif ?>
			<!-- <div class="pb40" style="color:<?= r(page('home')->coverColor()->isNotEmpty(), page('home')->coverColor(), '#fff') ?>;"><h1 class="title-h0"><?= r($page->coverCaption()->isNotEmpty(), $page->coverCaption(), $site->title()) ?></h1></div> -->
		</a>
	</div>
<?php endif ?>