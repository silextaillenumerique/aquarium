
<?php $items = $section->children()->listed() ?>
<?php if ($items->count() > 1): ?>
	<nav class="sidebar mt40 mb30 b b1 b0-sm" role="navigation">
		<div class="text--large pa10 bb b1 b0-sm">Sommaire</div>
		<ul class="pa10">
			<?php foreach ($items as $item): ?>
				<li class="mb5"><a href="/<?= $item->uri() ?>" class="inline-block unstyled <?= r($item->isActive() || $item->isAncestorOf($page), 'link-active') ?>"><?= $item->title()->widont() ?></a></li>
			<?php endforeach ?>
		</ul>
	</nav>
<?php endif ?>
