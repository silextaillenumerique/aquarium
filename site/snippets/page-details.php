<?php if ($page->details()->isNotEmpty()): ?>
	<div class="my60">
		<?php foreach ($page->details()->toStructure() as $detail): ?>
			<details class="py15">
				<summary><h2><?= $detail->title()->widont() ?></h2></summary>
				<div class="text mt15">
					<?= $detail->text()->kt() ?>
				</div>
			</details>
		<?php endforeach ?>
	</div>
<?php endif ?>