<footer class="px10 px20-sm px40-md pb40 pb60-md mt60 dark-mode">
	<div class="page-block pt30">
		<div class="columns">
			<div class="col col-12 col-6-sm col-4-md text">
				<?php if ($site->contacts()->isNotEmpty()): ?>
					<div class="row"><?= $site->contacts()->kt() ?></div>
				<?php endif ?>
			</div>

			<div class="col col-12 col-6-sm col-4-md text">
				<?php if ($site->informations()->isNotEmpty()): ?>
					<div class="row"><?= $site->informations()->kt() ?></div>
				<?php endif ?>
			</div>

			<div class="col col-12 col-6-sm col-4-md text">
				<?php if ($site->newsletterUrl()->isNotEmpty()): ?>
					<div class="row">
						<a href="<?= $site->newsletterUrl() ?>" target="_blank">S'inscrire à la newsletter</a>
					</div>
				<?php endif ?>
				<?php if ($site->facebookAccount()->isNotEmpty() || $site->instagramAccount()->isNotEmpty() || $site->vimeoAccount()->isNotEmpty()): ?>
					<div class="row">
						<?php if ($site->facebookAccount()->isNotEmpty()): ?>
							<a class="mr5 mb5" href="<?= $site->facebookAccount() ?>" target="_blank">Facebook</a>
						<?php endif ?>
						<?php if ($site->instagramAccount()->isNotEmpty()): ?>
							<a class="mr5 mb5" href="<?= $site->instagramAccount() ?>" target="_blank">Instagram</a>
						<?php endif ?>
						<?php if ($site->vimeoAccount()->isNotEmpty()): ?>
							<a class="mr5 mb5" href="<?= $site->vimeoAccount() ?>" target="_blank">Viméo</a>
						<?php endif ?>
					</div>
				<?php endif ?>

				<?php if (page('equipe') && page('equipe')->isDraft() === false): ?>
					<div class="row">
						<a href="<?= page('equipe')->url() ?>"><?= page('equipe')->title() ?></a>
					</div>
				<?php endif ?>
				<?php if (page('credits') && page('credits')->isDraft() === false): ?>
					<div class="row">
						<a href="<?= page('credits')->url() ?>"><?= page('credits')->title() ?></a>
					</div>
				<?php endif ?>
			</div>
		</div>
		<div class="mt30 pt20 private">
			<div class="text">
				<?php if ($site->private()->isNotEmpty()): ?>
					<?= $site->private()->kt() ?>
				<?php endif ?>
			</div>
		</div>
	</div>
</footer>


<?php /* echo Bnomei\Fingerprint::js('assets/script.js') */ ?>
</body>
</html>
