
<?php if (isset($items) && count($items)): ?>
	<div class="columns">
		<?php foreach ($items as $item): ?>
			<div class="col col-12 col-6-sm col-4-md mb20 relative">
				<a class="unstyled block pb20" href="<?= $item->url() ?>">
					<?php if ($item->picture()->isNotEmpty() && $image = $item->picture()->toFile()): ?>
						<figure class="shaded"><img src="<?= $image->focusCrop(900)->url() ?>" alt="<?= $image->description() ?>">
							<?php if ($item->mot()->isNotEmpty()): ?>
								<div class="mot upper title-h3">
									<span><?= $item->mot() ?></span>
								</div>
							<?php endif ?>
						</figure>
						<div class="pa5 pa0-md pt15-md grey-mode normal-mode-md">
							<?php if ($item->genre()->isNotEmpty()): ?>
							<div class="upper"><?= $item->genre() ?></div>
							<?php endif ?>
							<?php if ($item->dates()->isNotEmpty()): ?>
							<div class="row--extrasmall"><?= $item->dates() ?></div>
							<?php endif ?>
							<div class="row--extrasmall">
								<h3 class="title-h2"><?= $item->title()->widont() ?></h3>
							</div>
							<?php if ($item->accroche()->isNotEmpty()): ?>
							<div class="mt15"><?= $item->accroche()->kt() ?></div>
							<?php endif ?>
							<?php if ($item->avec()->isNotEmpty()): ?>
							<div><?= $item->avec()->kt() ?></div>
							<?php endif ?>
						</div>
					<?php endif ?>
				</a>
				<?php if ($item->btIsAccessible()->toBool() === true): ?>
				<div class="reserver">
					<?php if ($item->reservation()->isNotEmpty()): ?>
					<a href="<?= $item->reservation()->url() ?>" target="_blank">Réserver</a>
					<?php endif ?>
				</div>
				<?php endif ?>
			</div>
		<?php endforeach ?>
	</div>
<?php endif ?>