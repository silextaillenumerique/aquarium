<?php snippet('header') ?>

<?php if ($page->programmationIsAccessible()->toBool() === false && !$kirby->user()) {
	go($page->parent());
} ?>

<?php snippet('site-menu') ?>

<main id="site-content" class="pa10 pa20-sm pa40-md" role="main" >

	<div class="page-block">
		<div class="mb60">
			<div class="columns">
				<div class="col col-12 col-8-md">
					<header class="row--large">
						<div class="mb15"><?php snippet('breadcrumbs') ?></div>
						<h1 class="row"><?= $page->title()->widont() ?></h1>
						<?php if ($page->description()->isNotEmpty()): ?>
							<div class="text--large"><?= $page->description() ?></div>
						<?php endif ?>
						<?php if ($page->dates()->isNotEmpty()): ?>
							<div class="text--large"><?= $page->dates() ?></div>
						<?php endif ?>
					</header>
					<?php if ($page->isUnlisted()): ?>
						<?php snippet('page-text') ?>
					<?php endif ?>

					<!-- display calendar  -->
					<div class="display--calendar">
							<?php $calendar = new Mzur\KirbyCalendar\Calendar($page->events()->yaml()); ?>
							<?php
		   						snippet('calendar', [
		      								'calendar' => $calendar,
									      	'fields' => [
									         	'event_title' => t('titre'),
									         	'event_link' => t('lien'),
									         	'resa_link' => t('resa')
									      	]
		   						]);
							?>
					</div>  <!-- end display--calendar -->
				</div>
			</div>
		</div>

		<?php if ($page->children()->listed()->count()): ?>
			<?php if ($page->listTitle()->isNotEmpty()): ?>			
			<h2 class="mb15"><?= $page->listTitle()->widont() ?></h2>
			<?php endif ?>
			<?php $items = $page->children()->listed()->filter(function ($child) {
  				return $child->end()->toDate() > time();
			}); ?>
			<?php if ($items->count()): ?>
			<section>			
				<?php snippet('cards', ['items' => $items->sortBy('start', 'asc')]) ?>
			</section>
			<?php endif ?>
			<?php $itemsold = $page->children()->listed()->filter(function ($child) {
  				return $child->end()->toDate() < time();
			}); ?> 
			<?php if ($itemsold->count()): ?>
			<section>
				<h2 class="mb15">Dates passées</h3>
				<?php snippet('cards', ['items' => $itemsold->sortBy('start', 'asc')]) ?>
			</section>
			<?php endif ?>
		<?php endif ?>
	</div>
</main>
<?php snippet('footer') ?>
