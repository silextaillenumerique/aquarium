
<?php if ($page->animations()->isNotEmpty()): ?>
	<div class="row--extralarge">
		<h2 class="mb15">Autour du spectacle</h2>
		<div class="text">
			<?= $page->animations()->kt() ?>
		</div>
	</div>
<?php endif ?>