<?php if ($page->prices()->isNotEmpty()): ?>
	<div class="my60">
		<h2 class="mb15">Tarifs</h2>
		<div class="text">
			<?= $page->prices()->kt() ?>
		</div>
	</div>
<?php endif ?>