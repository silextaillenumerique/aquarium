<?php if ($page->related()->isNotEmpty() && $page->related()->toPages()->count()): ?>
	<a id="link-related"></a>
	<div class="mb40">
		<h2 class="mb20">Voir aussi le même soir</h2>
		<nav class="columns">
			<?php foreach ($page->related()->toPages() as $related): ?>
				<section class="col col-12 col-6-sm">
					<a class="unstyled block pa15 b b1" href="<?= $related->url() ?>">
						<div class="columns columns--small">	
							<div class="col col-12 col-2-sm mb20 mb0-sm">
								<?php if ($related->picture()->isNotEmpty() && $image = $related->picture()->toFile()): ?>
									<figure><img src="<?= $image->thumb('cropped_thumb')->url() ?>" alt="<?= $image->description() ?>"></figure>
								<?php endif ?>
							</div>
							<div class="col col-12 col-10-sm flex">
								<div class="grow">
									<h3 class="row--extrasmall"><?= $related->title()->widont() ?></h3>
									<?php if ($related->dates()->isNotEmpty()): ?>
										<div class="row--extrasmall"><?= $related->dates() ?></div>
									<?php endif ?>
									<?php if ($related->description()->isNotEmpty()): ?>
										<div class="text--large"><?= $related->description() ?></div>
									<?php endif ?>
								</div>
								<div class="ml15 flex align-bottom">
									<div class="button button--small rounded title-h3">+</div>
								</div>
							</div>
						</div>
					</a>
				</section>
			<?php endforeach ?>
		</nav>
	</div>
<?php endif ?>