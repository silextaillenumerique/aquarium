module.exports = {
  extends: ['eslint:recommended'],
  rules: {
    indent: ['error', 'tab'],
    quotes: [2, 'single', { avoidEscape: true }],
    semi: [2, 'never', { beforeStatementContinuationChars: 'any' }],
    'space-before-function-paren': ['error', { anonymous: 'never', named: 'never', asyncArrow: 'never' }]
  },
  env: {
    browser: true,
    node: true
  }
}
