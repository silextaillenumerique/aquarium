<?php snippet('header') ?>

<?php snippet('site-menu') ?>

<main id="site-content" class="pa10 pa20-sm" role="main" >

	<div class="page-block">
		<section class="columns mb60">
			<div class="col col-12 col-6-md mb20 pa40-md">
				<header class="row--large">
					<div class="mb15"><?php snippet('breadcrumbs') ?></div>
					<h1 class="row"><?= $page->title()->kt() ?></h1>
					<div class="metas row--extrasmall text--large">
						<?php if ($page->description()->isNotEmpty()): ?>
						<div class="text--large"><?= $page->description() ?></div>
						<?php endif ?>
						<?php if ($page->genre()->isNotEmpty()): ?>
							<div class="upper"><?= $page->genre() ?></div>
						<?php endif ?>
						<?php if ($page->dates()->isNotEmpty()): ?>
							<div class="relative"><?= $page->dates() ?></div>
						<?php endif ?>
						<?php if ($page->accroche()->isNotEmpty()): ?>
							<div class="mt40"><?= $page->accroche()->kt() ?></div>
						<?php endif ?>
						<?php if ($page->avec()->isNotEmpty()): ?>
							<div><?= $page->avec()->kt() ?></div>
						<?php endif ?>
					</div>
				</header>

				<?php snippet('page-text') ?>
				<?php snippet('page-details') ?>
				<?php snippet('page-tournee') ?>
				<?php snippet('page-animations-autre') ?>
				<?php snippet('page-links') ?>

			</div>

			<div class="col col-12 col-6-md">
				<?php snippet('page-embed') ?>
				<?php snippet('page-gallery') ?>
			</div>

		</section>

		<section class="mb40 mt100 related">
			<?php snippet('page-prevnext') ?>
		</section>
	</div>
</main>
<?php snippet('footer') ?>
