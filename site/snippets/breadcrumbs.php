<nav class="breadcrumb" aria-label="<?= t('label.breadcrumbs') ?>">
    <ul class="inline">
        <?php
            // removing homepage & current page
            $crumbs = $site->breadcrumb()->slice(1, $site->breadcrumb()->count() - 2);
            $index = 0;
        ?>
        <?php foreach ($crumbs  as $crumb): ?>
        <li class="inline-block mb5">
            <?php if ($crumb->isActive()): ?>
                <span><?= $crumb->title()->widont() ?></span>
            <?php else: ?>
                <a href="<?= $crumb->url() ?>"><?= $crumb->title()->widont() ?></a>
            <?php endif ?>
            <?php if ($index < $crumbs->count() - 1): ?>
                <span class="breadcrumbs-separator px5" aria-hidden="true">></span>
            <?php endif ?>
        </li>
        <?php $index += 1 ?>
        <?php endforeach ?>
    </ul>
</nav>