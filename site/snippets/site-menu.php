<header id="site-header" role="banner" aria-label="<?= t('label.header') ?>" class="py0 py20-md px0 px40-md">
	<div class="page-block">
		<div class="flex-md align-top">	
			<div class="flex mr0 mr20-md dark-mode normal-mode-md pa10 px20-sm pa0-md">
				<a href="<?= $site->url() ?>" class="unstyled text--large" rel="home" ><?= $site->title() ?></a>
				<button class="unstyled self-align-right my-5 hidden-md" onClick="(function(){
					    document.getElementById('menu').classList.toggle('hidden');
					})();">
					<img class="pa5" src="<?= asset('assets/images/icons/menu.svg')->url() ?>">
				</button>
			</div>
			<div id="menu" class="grow grey-mode normal-mode-md pa10 px20-sm pa0-md hidden visible-md">
				<div>
					<ul class="unstyled columns align-right-md">
						<?php foreach ($site->children()->listed()->filterBy('template', 'in', ['festival', 'section', 'default']) as $item): ?>
							<li class="col col-12 col-auto-sm no-grow">
								<a href="<?= $item->url() ?>" class="unstyled <?= r($item->isActive() || $item->isAncestorOf($page), 'link-active') ?>"><?= $item->title() ?></a>
							</li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>