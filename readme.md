# Kirby Starter Simple

This is a **simple** starter for working with Kirby 3.



## What is Kirby?

Kirby is a file-based CMS.
Easy to setup. Easy to use. Flexible as hell.

https://getkirby.com/



## Setup

Serve Kirby locally : 

```bash
php -d memory_limit=-1 -d upload_max_filesize=200M -d post_max_size=200M -S localhost:8787 kirby/router.php	
```



## Usage

Compile assets on the fly as you develop
```bash
npm run dev
```

Create an asset build ready for production

```bash
npm run build
```
