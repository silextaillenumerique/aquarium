<ul class="grid unstyled font-sans columns">
	<?php foreach ($files as $file): ?>
		<li class="grid__item col col-6 col-4-sm col-3-md col-2-lg mb15 mb30-md">
			<a href="<?= $file->url() ?>" class="unstyled block no-hover" target="_blank" download>
				<div class="mb10">
					<?php if ($file->type() == 'image'): ?>
						<figure class="grid__figure">
							<img src="<?= $file->thumb(['width' => 300, 'height' => 200])->url() ?>" alt="<?= $file->filename() ?>">
						</figure>
					<?php else: ?>
						<figure class="grid__icon">
							<div class="grid_icon__container">
								<div class="file-icon__wrapper">
									<div class="file-icon file-icon--large" data-type="<?= $file->extension() ?>"></div>
								</div>
							</div>
						</figure>
					<?php endif ?>
				</div>


				<div class="grid__item_caption">
					<div class="text--small"><span class="onhover"><?= $file->filename() ?></span></div>
					<div class="font-mono text--small"><span class="onhover"><?= $file->getWeight() ?></span></div>
					<?php if ($file->type() == 'image'): ?>
						<div class="font-mono text--small"><span class="onhover"><?= $file->dimensions() ?> pixels</span></div>
					<?php endif ?>

				</div>
			</a>
		</li>
	<?php endforeach ?>
</ul>