<?php if ($page->embed()->isNotEmpty() && $embed = $page->embed()->toEmbed()): ?>
	<figure class="row--medium video">
		<?= $embed->code() ?>
	</figure>
<?php endif ?>