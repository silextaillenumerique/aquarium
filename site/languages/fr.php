<?php

return [
    'code' => 'fr',
    'default' => true,
    'direction' => 'ltr',
    'locale' => [
        'fr_FR.UTF-8'
    ],
    'name' => 'Français',
    'translations' => [
        'date' => 'Agenda',
        'date_close' => 'Afficher l\'agenda',
        'date_open' => 'Fermer l\'agenda',
        'calendar-no-entry' => '',
        'calendar-full-time-format' => '%Hh%M',
        'calendar-day-format' => '%a %e/%m',
        'calendar-time-format' => '%B %y',
    ],
    'url' => '/'
];