<?php if ($page->tournee()->isNotEmpty()): ?>
	<div class="my40 bb b1">
		<h2 class="mb15">Tournée</h2>
		<?php $tournees = $page->tournee()->toStructure()->filter(function ($child) {
  				return $child->end()->toDate() > time();
			}); ?>
		<?php foreach ($tournees->sortBy('start', 'asc') as $tournee): ?>
			<div class="flex bt b1 py5">
				<div class="no-shrink mr10 py5 text--large">
					<div><?= $tournee->date() ?></div>
					<div><?= $tournee->lieu() ?></div>
				</div>
				<?php if ($tournee->link()->isNotEmpty()): ?>
				<div class="self-align-right self-align-center">
					<a href="<?= $tournee->link() ?>" target="_blank" class="unstyled block"><span class="button button--small valid">Plus d'infos</span></a>
				</div>
				<?php endif ?>
			</div>
		<?php endforeach ?>
		<?php $tourneesold = $page->tournee()->toStructure()->filter(function ($child) {
  				return $child->end()->toDate() < time();
		}); ?>
		<h3 class="mt15 mb10">Dates passées</h3>
		<?php foreach ($tourneesold->sortBy('start', 'desc') as $tournee): ?>
			<div class="flex bt b1 py5">
				<div class="no-shrink mr10 py5 text--large">
					<div><?= $tournee->date() ?></div>
					<div><?= $tournee->lieu() ?></div>
				</div>
			</div>
		<?php endforeach ?>
	</div>
<?php endif ?>