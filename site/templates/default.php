<?php snippet('header') ?>
<?php snippet('site-menu') ?>

<main id="site-content" class="pa10 pa20-sm pa40-md" role="main" >
	<div class="page-block">
		<header class="row--large">
			<h1><?= $page->title()->widont() ?></h1>
		</header>
		<?php snippet('page-text') ?>
	</div>
</main>
<?php snippet('footer') ?>
