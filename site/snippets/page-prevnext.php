
<?php if ($page->hasPrevListed() || $page->hasNextListed()): ?>
	<nav class="flex pt40 wrap no-wrap-md">
		<?php if ($page->hasPrevListed()): ?>
			<div class="flex vertical-center nowrap">
				<a class="title-h3 unstyled flex no-wrap vertical-center" href="<?= $page->prevListed()->url() ?>">
					<span class="title-h2">←</span>&nbsp;<?= $page->prevListed()->title()->widont() ?>
				</a>
			</div>
		<?php endif ?>

		<?php if ($page->hasNextListed()): ?>
			<div class="flex vertical-center self-align-right align-right nowrap" >
				<a class="title-h3 unstyled ml15 flex no-wrap vertical-center" href="<?= $page->nextListed()->url() ?>">
					<?= $page->nextListed()->title()->widont() ?>&nbsp;<span class="title-h2">→</span>
				</a>
			</div>
		<?php endif ?>
	</nav>
<?php endif ?>
